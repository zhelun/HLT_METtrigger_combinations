# HLT METtrigger Combinations

This is the project housing the code for NN studies of the MET trigger. Most of the code are in HLT_combination.ipynb

To properly run it, please make sure that the relevant libraries in HLT_combination.ipynb are installed. Since this project is to combine HLT triggers, make sure that the input root files contains the HLT triggers needed as input.

In the end, the single neuron regression model's parameters were saved and immigrated to athena. The athena-based ML is achieved through lwtNN and is tested here:
    https://gitlab.cern.ch/zhelun/hlttrigger_mlathena/-/tree/master/
